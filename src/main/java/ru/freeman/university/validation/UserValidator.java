package ru.freeman.university.validation;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import ru.freeman.university.dao.interfaces.UserDao;
import ru.freeman.university.models.User;

@Component
public class UserValidator implements Validator {
	
	@Autowired
	UserDao data;

	@Override
	public boolean supports(Class<?> clazz) {
		
		return User.class.isAssignableFrom(clazz);
	}

	@Override
	public void validate(Object target, Errors errors) {
		
		User user=(User)target;
		
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "login", "login is empty", "enter login");
		
		String login=user.getLogin();
		
		if(login.length()<3) errors.rejectValue("login", "login is short", "login must be between 3 and 25 characters");
		if(login.length()>25) errors.rejectValue("login", "login is long", "login must be between 3 and 25 characters");
		if(data.isExist(login)) errors.rejectValue("login", "login already exist", "this login already exist");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "password", "password is empty", "enter password");
		
		String password=user.getPassword();
		
		if(password.length()<4) errors.rejectValue("password", "password is short", "password must be between 4 and 25 characters");
		if(password.length()>25) errors.rejectValue("password", "password is long", "password must be between 4 and 25 characters");
		
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "status", "status is emtpy", "enter status");
		
		String status=user.getStatus();
		
		if(!status.equals("admin")&&!status.equals("student")&&!status.equals("examiner"))  {
			errors.rejectValue("status", "status is wrong", "status is wrong");
		}
		
	}

}
