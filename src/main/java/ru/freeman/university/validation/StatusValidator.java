package ru.freeman.university.validation;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import ru.freeman.university.dao.interfaces.UserDao;
import ru.freeman.university.models.User;

@Component
public class StatusValidator implements Validator {
	
	@Autowired
	UserDao data;

	@Override
	public boolean supports(Class<?> clazz) {
		
		return User.class.isAssignableFrom(clazz);
	}

	@Override
	public void validate(Object target, Errors errors) {
		
		User user=(User)target;
		String status=data.getUserByLogin(user.getLogin()).getStatus();

		if(status.equals("admin")&&data.countAdmin()<2) {
			errors.rejectValue("status", "last admin", "must be minimum one admin");
		}
		
	}
	
	

}
