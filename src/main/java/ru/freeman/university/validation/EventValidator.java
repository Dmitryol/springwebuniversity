package ru.freeman.university.validation;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import ru.freeman.university.dao.interfaces.ScheduleDao;
import ru.freeman.university.models.Event;

@Component
public class EventValidator implements Validator {
	
	@Autowired
	private ScheduleDao data;

	@Override
	public boolean supports(Class<?> clazz) {
		
		return Event.class.isAssignableFrom(clazz);
	}

	@Override
	public void validate(Object target, Errors errors) {
		
		Event event=(Event)target;
		
		if(data.isEventExist(event.getStudent(), event.getExam())) errors.rejectValue("id", "event already exist", "event with selected student and exam already exist");
		
		ValidationUtils.rejectIfEmpty(errors, "status", "status is empty");
	}

}
