package ru.freeman.university.validation;

import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import ru.freeman.university.models.Exam;

@Component
public class ExamValidator implements Validator {

	@Override
	public boolean supports(Class<?> clazz) {
		
		return Exam.class.isAssignableFrom(clazz);
	}

	@Override
	public void validate(Object target, Errors errors) {
		
		Exam exam=(Exam)target;
		
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "subject", "subject is empty", "enter subject");
		
		String subject=exam.getSubject();
		
		if(subject.length()<3) errors.rejectValue("subject", "subject is short", "subject  must be between 3 and 30 characters");
		if(subject.length()>30) errors.rejectValue("subject", "subject is long", "subject must be between 3 and 30 characters");
	}

}
