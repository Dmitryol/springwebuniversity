package ru.freeman.university.validation;

import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import ru.freeman.university.models.Student;

@Component
public class StudentValidator implements Validator{

	@Override
	public boolean supports(Class<?> clazz) {
	
		return Student.class.isAssignableFrom(clazz);
	}

	@Override
	public void validate(Object target, Errors errors) {
		
		Student student=(Student)target;
		
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "name", "name is empty", "enter name");
		
		String name=student.getName();
		
		if(name.length()<3) errors.rejectValue("name", "name is short", "name must be between 3 and 20 characters");
		if(name.length()>20) errors.rejectValue("name", "name is long", "name must be between 3 and 20 characters");
		
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "surname", "surname is empty", "enter surname");
		
		String surname=student.getSurname();
		
		if(surname.length()<4) errors.rejectValue("surname", "name is short", "name must be between 4 and 30 characters");
		if(surname.length()>30) errors.rejectValue("surname", "name is long", "name must be between 4 and 30 characters");
	
		String group=student.getGroup();
	
		if(group!=null) {
			if(group.length()>10) errors.rejectValue("group", "group is long", "group must be less 10 characters");
		}
		
	}

}
