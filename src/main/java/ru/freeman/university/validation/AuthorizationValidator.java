package ru.freeman.university.validation;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import ru.freeman.university.dao.interfaces.UserDao;
import ru.freeman.university.models.User;

@Component
public class AuthorizationValidator  implements Validator{
	
	@Autowired
	private UserDao data;
	

	@Override
	public boolean supports(Class<?> clazz) {
		return User.class.isAssignableFrom(clazz);
	}

	@Override
	public void validate(Object target, Errors errors) {
		
		User user=(User)target;
		
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "login", "login is empty", "enter login");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "password", "password is empty", "enter password");
		
		if(data.isExist(user.getLogin())) {
			
			String password=data.getUserByLogin(user.getLogin()).getPassword();
			if(password.equals(user.getPassword())==false) {
				errors.rejectValue("password", "password is wrong", "password is wrong");
			}
			
		}else {
			errors.rejectValue("login", "login don't exist", "login is worn");
		}
		
		
		
	}

}
