package ru.freeman.university.controllers;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import ru.freeman.university.dao.interfaces.ExamDao;
import ru.freeman.university.dao.interfaces.ScheduleDao;
import ru.freeman.university.dao.interfaces.StudentDao;
import ru.freeman.university.models.Event;
import ru.freeman.university.models.Exam;
import ru.freeman.university.models.Student;
import ru.freeman.university.validation.EventValidator;

@Controller
public class ScheduleController {
	
	@Autowired
	private ScheduleDao eventDao;
	@Autowired
	StudentDao studDao;
	@Autowired
	ExamDao examDao;
	@Autowired
	EventValidator validator;
	
	@RequestMapping(value = "/schedule/show", method = RequestMethod.GET)
	public String showScheduleExams(Model model, HttpServletRequest request) {
		
		List<Event> events=eventDao.getAllEvents();
		model.addAttribute("events", events);
		List<Student> students=studDao.getAllStudents();
		List<Exam> exams=examDao.getAllExams();
		model.addAttribute("students", students);
		model.addAttribute("exams", exams);
		Event event=new Event();
		model.addAttribute("event",event);
		if(!request.getSession().getAttribute("status").equals("student")) {
			return "schedule";
		}else {
			return "scheduleStud";
		}
		
	
		
	}
	@RequestMapping(value = "/schedule/show", method = RequestMethod.POST)
	public String showScheduleExams(Event event, BindingResult errors, Model model) {
		
		validator.validate(event, errors);
		if(errors.hasErrors()) {
			if(errors.hasFieldErrors("id")) {
				model.addAttribute("error", errors.getFieldError("id").getDefaultMessage());
			}
		}
		else {
			
			eventDao.addEvent(event);
		}
		return "redirect:/schedule/show";
	}
	@RequestMapping (value = "/schedule/delete", method = RequestMethod.POST )
	public String deleteEvent(HttpServletRequest request) {
		
		int id=Integer.parseInt(request.getParameter("id"));
		eventDao.deleteEvent(id);
		
		return "redirect:/schedule/show";
		
	}

}
