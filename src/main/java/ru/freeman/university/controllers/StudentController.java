package ru.freeman.university.controllers;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import ru.freeman.university.dao.interfaces.StudentDao;
import ru.freeman.university.models.Student;
import ru.freeman.university.validation.StudentValidator;

@Controller
public class StudentController {
	
	@Autowired
	StudentDao data;
	@Autowired
	StudentValidator studValidator;
	
	@RequestMapping(value = "/student/all", method = RequestMethod.GET)
	public String showAllStudents(Model model) {
		
		List<Student> students=data.getAllStudents();
		model.addAttribute("students", students);
		return "students";
	}
	@RequestMapping(value = "/student/create", method = RequestMethod.GET)
	public String createStudent(Model model) {
		
		Student student = new Student();
		model.addAttribute("student", student);
		return "createStudent";
	}
	@RequestMapping(value = "/student/create", method = RequestMethod.POST)
	public String createStudent(Student student, BindingResult errors, Model model) {
		
		String name_error;
		String surname_error;
		String group_error;
		
		studValidator.validate(student, errors);
		
		if(errors.hasErrors()) {
			
			if(errors.hasFieldErrors("name")) {
				
				name_error=errors.getFieldError("name").getDefaultMessage();
				model.addAttribute("name_error", name_error);
			}
			if(errors.hasFieldErrors("surname")) {
				
				surname_error = errors.getFieldError("surname").getDefaultMessage();
				model.addAttribute("surname_error", surname_error);
			}
			if(errors.hasFieldErrors("group")) {
				
				group_error= errors.getFieldError("group").getDefaultMessage();
				model.addAttribute("group_error", group_error);
			}
			
			return "createStudent";
		}else {
			data.createStudent(student);
			return "redirect:/student/all";
		}
	}
	@RequestMapping(value = "/student/*", method = RequestMethod.GET)
    public String showStudent(Model model, HttpServletRequest request) {
		
		int id=Integer.parseInt(request.getRequestURI().substring(20));
		Student student=data.getStudentById(id);
		model.addAttribute("student", student);
		
		return "student";
    	
    }
	@RequestMapping(value = "/student/delete", method = RequestMethod.POST)
	public String deleteUser(Student student) {
		
		data.deleteStudentById(student.getId());
		
		return "redirect:/student/all";
	}
	@RequestMapping(value ="/student/setName", method = RequestMethod.POST)
	public String setName(Student student, BindingResult errors,Model model, HttpServletRequest request) {
		
		Student new_stud=data.getStudentById(student.getId());
		String name=request.getParameter("title");
		new_stud.setName(name);
		studValidator.validate(new_stud, errors);
	    if(errors.hasFieldErrors("name")) {
			model.addAttribute("name_error", errors.getFieldError("name").getDefaultMessage());
			new_stud.setName(student.getName());
			model.addAttribute("student", new_stud);
			return "student";
		}else {
		      data.updateStudent(student.getId(),new_stud);
		      model.addAttribute("student", new_stud);
		      return "student";
		}     

	}
	@RequestMapping(value ="/student/setSurname", method = RequestMethod.POST)
	public String setSurname(Student student, BindingResult errors,Model model, HttpServletRequest request) {
		
		Student new_stud=data.getStudentById(student.getId());
		String surname=request.getParameter("title");
		new_stud.setSurname(surname);
		studValidator.validate(new_stud, errors);
	    if(errors.hasFieldErrors("surname")) {
			model.addAttribute("surname_error", errors.getFieldError("surname").getDefaultMessage());
			new_stud.setSurname(student.getSurname());
			model.addAttribute("student", new_stud);
			return "student";
		}else {
		      data.updateStudent(student.getId(),new_stud);
		      model.addAttribute("student", new_stud);
		      return "student";
		}     

	}
	@RequestMapping(value ="/student/setGroup", method = RequestMethod.POST)
	public String setGroup(Student student, BindingResult errors,Model model, HttpServletRequest request) {
		
		Student new_stud=data.getStudentById(student.getId());
		String group=request.getParameter("title");
		new_stud.setGroup(group);
		studValidator.validate(new_stud, errors);
	    if(errors.hasFieldErrors("group")) {
			model.addAttribute("group_error", errors.getFieldError("group").getDefaultMessage());
			new_stud.setGroup(student.getGroup());
			model.addAttribute("student", new_stud);
			return "student";
		}else {
		      data.updateStudent(student.getId(),new_stud);
		      model.addAttribute("student", new_stud);
		      return "student";
		}     

	}
	

}
