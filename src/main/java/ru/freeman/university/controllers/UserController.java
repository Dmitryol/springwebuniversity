package ru.freeman.university.controllers;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import ru.freeman.university.dao.interfaces.UserDao;
import ru.freeman.university.models.User;
import ru.freeman.university.validation.StatusValidator;
import ru.freeman.university.validation.UserValidator;

/**
 * Handles requests for the application home page.
 */
@Controller
public class UserController {
	
	@Autowired
	UserDao data;
	@Autowired
	UserValidator userValidator;
	@Autowired
	StatusValidator statusValidator;
	
	@RequestMapping(value="/user/create", method = RequestMethod.GET)
	public String createUser(Model model) {
		User user=new User();
		model.addAttribute("user", user);
		return "createUser";
	}
	@RequestMapping(value="/user/create", method = RequestMethod.POST)
	public String createUser(User user, BindingResult errors, Model model) {
		
		String login_error;
		String password_error;
		String status_error;
		
		userValidator.validate(user, errors);
		
		if(errors.hasErrors()) {
			if(errors.hasFieldErrors("login")) {
				login_error=errors.getFieldError("login").getDefaultMessage();
				model.addAttribute("login_error", login_error);
			}
			if(errors.hasFieldErrors("password")) {
				password_error=errors.getFieldError("password").getDefaultMessage();
				model.addAttribute("password_error", password_error);
			}
			if(errors.hasFieldErrors("status")) {
				status_error=errors.getFieldError("status").getDefaultMessage();
				model.addAttribute("status_error", status_error);
			}
			return "createUser";
			
		}else {
			
		    data.createUser(user);
		    return "redirect:/user/all";
		}
	} 
	@RequestMapping(value = "/user/all",method = RequestMethod.GET )
	public String showAllUsers(Model model) {
		
		List<User> users;
		users=data.getAllUsers();
		model.addAttribute("users", users);
		return "users";
	}
	@RequestMapping(value = "/user/*", method = RequestMethod.GET)
	public String showUser(Model model, HttpServletRequest request) {
		

		String login=request.getRequestURI().substring(17);
		User user=data.getUserByLogin(login);
		model.addAttribute("user", user);
		User test=new User();
		model.addAttribute("test", test);

		return "user";
	}
	@RequestMapping(value = "/user/delete", method = RequestMethod.POST)
	public String deleteUser(User user, BindingResult errors, Model model) {
		
		statusValidator.validate(user, errors);
		if(errors.hasErrors()) {
			model.addAttribute("delete_error", "you don't must delete last admin");
			return "user";
			
		}else {
		    data.deleteUserByLogin(user.getLogin());
		    return "redirect:/user/all";
		}    
	}
	@RequestMapping(value ="/user/setLogin", method = RequestMethod.POST)
	public String setLogin(User user, BindingResult errors,Model model, HttpServletRequest request) {
		
		User new_user=data.getUserByLogin(user.getLogin());
		String login=request.getParameter("title");
		System.out.println(login);
		new_user.setLogin(login);
		userValidator.validate(new_user, errors);
	    if(errors.hasFieldErrors("login")) {
			model.addAttribute("login_error", errors.getFieldError("login").getDefaultMessage());
			new_user.setLogin(user.getLogin());
			model.addAttribute("user", new_user);
			return "user";
		}else {
		      data.updeteUser(user.getLogin(), new_user);
		      model.addAttribute("user", new_user);
		      return "user";
		}     

	}
	@RequestMapping(value ="/user/setPassword", method = RequestMethod.POST)
	public String setPassword(User user, BindingResult errors,Model model, HttpServletRequest request) {
		
		User new_user=data.getUserByLogin(user.getLogin());
		String pwd=request.getParameter("title");
		new_user.setPassword(pwd);
		userValidator.validate(new_user, errors);
		if(errors.hasFieldErrors("password")) {
			  model.addAttribute("password_error", errors.getFieldError("password").getDefaultMessage());
			  new_user.setPassword(user.getPassword());
			  model.addAttribute("user", new_user);
			  return "user";
			
			
		}else {
		      data.updeteUser(user.getLogin(), new_user);
		      model.addAttribute("user", new_user);
		      return "user";
		}     

	}
	@RequestMapping(value ="/user/setStatus", method = RequestMethod.POST)
	public String setStatus(User user, BindingResult errors,Model model, HttpServletRequest request) {
		
		User new_user=data.getUserByLogin(user.getLogin());
		String status=request.getParameter("status");
		System.out.println(status);
		new_user.setStatus(status);
		statusValidator.validate(new_user, errors);
		if(errors.hasFieldErrors("status")) {
			  model.addAttribute("status_error", errors.getFieldError("status").getDefaultMessage());
			  new_user.setStatus(data.getUserByLogin(user.getLogin()).getStatus());
			  model.addAttribute("user", new_user);
			  return "user";
			
			
		}else {
		      data.updeteUser(user.getLogin(), new_user);
		      model.addAttribute("user", new_user);
		      return "user";
		}     

	}
}
