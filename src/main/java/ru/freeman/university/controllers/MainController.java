package ru.freeman.university.controllers;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import ru.freeman.university.dao.interfaces.UserDao;
import ru.freeman.university.models.User;
import ru.freeman.university.validation.AuthorizationValidator;

@Controller
public class MainController {
	
	@Autowired
	UserDao data;
	@Autowired
	AuthorizationValidator authorizationValidator;
	
	@RequestMapping(value = "/", method = RequestMethod.GET)
	public String main(Model model, HttpServletRequest request) {
				
		
		HttpSession session=request.getSession(false);
		if(session==null) return "main";
		else {
			model.addAttribute("status", session.getAttribute("status"));
			return "welcome";
		}
	}
	@RequestMapping(value ="/login", method = RequestMethod.GET)
	public String valid(ModelMap model) {
		User user=new User();
		model.put("user", user);
		return "login";
	}
	@RequestMapping(value ="/login", method = RequestMethod.POST)
	public String result(User user, BindingResult result, Model model, HttpServletRequest request) {
		
		String login_error;
		String password_error;
		
		authorizationValidator.validate(user, result);
		
		if(result.hasErrors()) {
			if(result.hasFieldErrors("login")) {
				login_error=result.getFieldError("login").getDefaultMessage();
				model.addAttribute("login_error", login_error);
			}
			if(result.hasFieldErrors("password")) { 
				password_error=result.getFieldError("password").getDefaultMessage();
	            model.addAttribute("password_error",password_error);
			}
			return "login";
		}
		else {
			
			HttpSession session=request.getSession();
			session.setMaxInactiveInterval(60*30);
			session.setAttribute("login", user.getLogin());
			session.setAttribute("status", data.getUserByLogin(user.getLogin()).getStatus());
			return "redirect:";
		}
	}
	@RequestMapping(value ="/logout", method = RequestMethod.GET)
	public String welcome(HttpServletRequest request) {
		
		HttpSession session=request.getSession(false);
		
		if(session!=null) session.invalidate();

		return "redirect:";
	}
	

}
