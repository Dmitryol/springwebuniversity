package ru.freeman.university.controllers;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import ru.freeman.university.dao.interfaces.ExamDao;
import ru.freeman.university.models.Exam;
import ru.freeman.university.validation.ExamValidator;

@Controller
public class ExamController {
	
	@Autowired
	ExamDao data;
	@Autowired
	ExamValidator examValidator;
	
	@RequestMapping(value = "/exam/all", method= RequestMethod.GET)
	public String showAllExams(Model model) {
		
		List<Exam> exams =data.getAllExams();
		model.addAttribute("exams", exams);
		
		return "exams";
	}
	@RequestMapping(value = "/exam/create", method = RequestMethod.GET)
	public String createStudent(Model model) {
		
		Exam exam = new Exam();
		model.addAttribute("exam", exam);
		return "createExam";
	}
	@RequestMapping(value = "/exam/create", method = RequestMethod.POST)
	public String createStudent(Exam exam, BindingResult errors, Model model, HttpServletRequest request) {
		
		String subject_error;
		
		examValidator.validate(exam, errors);
		
		if(errors.hasErrors()) {
			
			if(errors.hasFieldErrors("subject")) {
				subject_error=errors.getFieldError("subject").getDefaultMessage();
				model.addAttribute("subject_error", subject_error);
			}
			return "createExam";
			
		}else { 
		    
			data.createExam(exam);
		    return "redirect:/";
		}
	}
	@RequestMapping(value = "/exam/*", method = RequestMethod.GET)
	public String showExam(Model model, HttpServletRequest request) {
		
		int id=Integer.parseInt(request.getRequestURI().substring(17));
		
		Exam exam=data.getExamById(id);
		model.addAttribute("exam", exam);
		
		return "exam";
	}
	@RequestMapping(value = "/exam/delete", method = RequestMethod.POST)
	public String deleteExam(Exam exam) {
		
		data.deleteExamById(exam.getId());
		
		return "redirect:/";
		
	}
	@RequestMapping(value ="/exam/setSubject", method = RequestMethod.POST)
	public String setSubject(Exam exam, BindingResult errors,Model model, HttpServletRequest request) {
		
		Exam new_exam=data.getExamById(exam.getId());
		String subject=request.getParameter("title");
		new_exam.setSubject(subject);
		examValidator.validate(new_exam, errors);
	    if(errors.hasFieldErrors("subject")) {
			model.addAttribute("subject_error", errors.getFieldError("subject").getDefaultMessage());
			new_exam.setSubject(exam.getSubject());
			model.addAttribute("exam", new_exam);
			return "exam";
		}else {
		      data.updateExam(exam.getId(), new_exam);
		      model.addAttribute("exam", new_exam);
		      return "exam";
		}     

	}
	@RequestMapping(value ="/exam/setDate", method = RequestMethod.POST)
	public String setDate(Exam exam, BindingResult errors,Model model, HttpServletRequest request) {
		
		Exam new_exam=data.getExamById(exam.getId());
		String date=request.getParameter("title");
		new_exam.setDate(date);
		examValidator.validate(new_exam, errors);
	    if(errors.hasFieldErrors("date")) {
			model.addAttribute("date_error", errors.getFieldError("date").getDefaultMessage());
			new_exam.setDate(exam.getDate());
			model.addAttribute("exam", new_exam);
			return "exam";
		}else {
		      data.updateExam(exam.getId(), new_exam);
		      model.addAttribute("exam", new_exam);
		      return "exam";
		}     

	}

}
