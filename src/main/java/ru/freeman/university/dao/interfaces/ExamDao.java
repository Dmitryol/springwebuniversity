package ru.freeman.university.dao.interfaces;

import java.util.List;

import ru.freeman.university.models.Exam;

public interface ExamDao {
	
	public void createExam(Exam exam); 
	public Exam getExamById(int id);
	public void deleteExamById(int id);
	public List<Exam> getAllExams();
	public void updateExam(int id, Exam exam);
	

}
