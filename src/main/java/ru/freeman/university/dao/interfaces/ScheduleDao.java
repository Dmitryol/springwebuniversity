package ru.freeman.university.dao.interfaces;

import java.util.List;

import ru.freeman.university.models.Event;
import ru.freeman.university.models.Exam;
import ru.freeman.university.models.Student;

public interface ScheduleDao {
	
	public void addEvent(Event event);
	public Event getEventById(int id);
	public void deleteEvent(int id);
	public List<Event> getAllEvents();
	public void updateEvent(Event event, int id);
	public boolean isEventExist(Student student, Exam exam);

}
