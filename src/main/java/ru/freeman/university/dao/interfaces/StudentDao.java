package ru.freeman.university.dao.interfaces;

import java.util.List;

import ru.freeman.university.models.Student;

public interface StudentDao {
	
	public void createStudent(Student student);
	public void deleteStudentById(int id);
	public Student getStudentById(int id);
	public List<Student> getAllStudents();
	public void updateStudent(int id, Student student);

}
