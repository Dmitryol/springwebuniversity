package ru.freeman.university.dao.interfaces;

import java.util.List;

import ru.freeman.university.models.User;

public interface UserDao {
	
	public void createUser(User user);
	public void deleteUserByLogin(String login);
	public void updeteUser(String login, User user);
	public User getUserByLogin(String login);
	public List<User> getAllUsers();
	public boolean isExist(String login);
	public int countAdmin();

}
