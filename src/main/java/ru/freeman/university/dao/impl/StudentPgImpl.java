package ru.freeman.university.dao.impl;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import ru.freeman.university.dao.interfaces.StudentDao;
import ru.freeman.university.models.Student;

@Component
public class StudentPgImpl implements StudentDao {
	
private JdbcTemplate jdbcTemplate;
	
	@Autowired
	public void setDataSource(DataSource dataSource) {
		
		this.jdbcTemplate=new JdbcTemplate(dataSource);
		
	}

	@Override
	public void createStudent(Student student) {
		
		String sql="INSERT INTO students (name, surname, group_number) VALUES ('"
		+student.getName()+"','"
		+student.getSurname()+"','"
		+student.getGroup()+"')";
		jdbcTemplate.execute(sql);
		
	}

	@Override
	public void deleteStudentById(int id) {
		
		String sql="DELETE FROM students WHERE id="+id+";";
		jdbcTemplate.execute(sql);
		
	}

	@Override
	public Student getStudentById(int id) {
		
		String sql="SELECT * FROM students WHERE id="+id+";";
		return jdbcTemplate.queryForObject(sql, new StudentRowMapper());
	}

	@Override
	public List<Student> getAllStudents() {
		
		String sql="SELECT * FROM students;";
		List<Student> students=jdbcTemplate.query(sql, new StudentRowMapper());
		return students;
	}

	@Override
	public void updateStudent(int id, Student student) {
		
		String sql="UPDATE students SET name='"+student.getName()+
				"', surname= '"+student.getSurname()+
				"', group_number= '"+student.getGroup()+
				"' WHERE id = '"+id+"';";
		jdbcTemplate.execute(sql);
		
	}
	private class StudentRowMapper implements RowMapper<Student> {

		@Override
		public Student mapRow(ResultSet rs, int row) throws SQLException {
			
			Student student=new Student();
			student.setId(rs.getInt("id"));
			student.setName(rs.getString("name"));
			student.setSurname(rs.getString("surname"));
			student.setGroup(rs.getString("group_number"));
			return student;
		}
		
	}


}
