package ru.freeman.university.dao.impl;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import ru.freeman.university.dao.interfaces.UserDao;
import ru.freeman.university.models.User;

@Component
public class UserPgImpl implements UserDao {
	
	private JdbcTemplate jdbcTemplate;
	
	@Autowired
	public void setDataSource(DataSource dataSource) {
		
		this.jdbcTemplate=new JdbcTemplate(dataSource);
		
	}

	@Override
	public void createUser(User user) {
		
		String sql="INSERT INTO users (login, password, status) VALUES ('"+user.getLogin()+"','"+user.getPassword()+"','"+user.getStatus()+"');";
		jdbcTemplate.execute(sql);

	}

	@Override
	public void deleteUserByLogin(String login) {
		
		String sql="DELETE FROM users WHERE login='"+login+"';";
		jdbcTemplate.execute(sql);

	}

	@Override
	public void updeteUser(String login, User user) {
		
		String sql="UPDATE users SET login='"+user.getLogin()
		          +"', password='"+user.getPassword()
		          +"', status='"+user.getStatus()
		          +"' WHERE login='"+login+"'";
		
		
		jdbcTemplate.execute(sql);

	}

	@Override
	public User getUserByLogin(String login) {
		
		String sql="SELECT * FROM users WHERE login='"+login+"';";
		return jdbcTemplate.queryForObject(sql, new UserRowMapper());
	}

	@Override
	public List<User> getAllUsers() {
		
		String sql="SELECT * FROM users";
		List<User> users=jdbcTemplate.query(sql, new UserRowMapper());
		return users;
	}
	private class UserRowMapper implements RowMapper<User> {

		@Override
		public User mapRow(ResultSet rs, int row) throws SQLException {
			
			User user=new User();
			user.setLogin(rs.getString("login"));
			user.setPassword(rs.getString("password"));
			user.setStatus(rs.getString("status"));
			return user;
		}
		
	}
	@Override
	public boolean isExist(String login) {
		
		String sql="SELECT COUNT(login) FROM users WHERE login='"+login+"';";
		int result= jdbcTemplate.queryForInt(sql);
		if(result >= 1) return true;
		else return false;
	}

	@Override
	public int countAdmin() {
		
		String sql="SELECT COUNT(login) FROM users WHERE status ='admin'";
		return jdbcTemplate.queryForInt(sql);
	}

}
