package ru.freeman.university.dao.impl;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import ru.freeman.university.dao.interfaces.ExamDao;
import ru.freeman.university.models.Exam;

@Component
public class ExamPgImpl implements ExamDao {
	
	private JdbcTemplate jdbcTemplate;
	
    @Autowired
	public void setDataSource(DataSource dataSource) {
		
		this.jdbcTemplate=new JdbcTemplate(dataSource);
	}
	
	@Override
	public void createExam(Exam exam) {
		
		String sql="INSERT INTO exams (subject, date) VALUES ('"+exam.getSubject()+"','"+exam.getDate()+"')";
	    jdbcTemplate.execute(sql);
		
	}

	@Override
	public Exam getExamById(int id) {
		
		String sql="SELECT * FROM exams WHERE id="+id+";";
		return jdbcTemplate.queryForObject(sql, new ExamRowMapper());
	}

	@Override
	public void deleteExamById(int id) {
		
		String sql="DELETE FROM exams WHERE id="+id+";"; 
		jdbcTemplate.execute(sql);
		
	}

	@Override
	public List<Exam> getAllExams() {
		
		String sql="SELECT * FROM exams;";
		List<Exam> exams=jdbcTemplate.query(sql, new ExamRowMapper());
		return exams;
	}

	@Override
	public void updateExam(int id, Exam exam) {
		
		String sql="UPDATE exams SET subject='"+exam.getSubject()+
				"', date ='"+exam.getDate()+"' WHERE id="+id+";";
		
		jdbcTemplate.execute(sql);
		
	}
	private class ExamRowMapper implements RowMapper<Exam> {

		@Override
		public Exam mapRow(ResultSet rs, int arg1) throws SQLException {
			
			Exam exam=new Exam();
			exam.setId(rs.getInt("id"));
			exam.setSubject(rs.getString("subject"));
			exam.setDate(rs.getString("date"));
			return exam;
		}
		
	}

}
