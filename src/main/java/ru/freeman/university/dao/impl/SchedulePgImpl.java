package ru.freeman.university.dao.impl;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import ru.freeman.university.dao.interfaces.ExamDao;
import ru.freeman.university.dao.interfaces.ScheduleDao;
import ru.freeman.university.dao.interfaces.StudentDao;
import ru.freeman.university.models.Event;
import ru.freeman.university.models.Exam;
import ru.freeman.university.models.Student;

@Component
public class SchedulePgImpl implements ScheduleDao{
	
	@Autowired
	StudentDao studDao;
	@Autowired
	ExamDao examDao;
	
	private JdbcTemplate jdbcTemplate;
	
	@Autowired
	public void setDataSource(DataSource dataSource) {
		this.jdbcTemplate=new JdbcTemplate(dataSource);
	}

	@Override
	public void addEvent(Event event) {
		
		String sql="INSERT INTO schedule (student_id, exam_id, status, score) VALUES ('"+
		     event.getStudent().getId()+"','"+
		     event.getExam().getId()+"','"+
		     event.getStatus()+"','"+
		     event.getScore()+"');";
		jdbcTemplate.execute(sql);
		
	}

	@Override
	public Event getEventById(int id) {
		
		String sql="SELECT * FROM schedule WHERE id="+id+";";
		return jdbcTemplate.queryForObject(sql, new EventRowMapper());
	}

	@Override
	public void deleteEvent(int id) {
		
	    String sql="DELETE FROM schedule WHERE id="+id+";";
	    jdbcTemplate.execute(sql);
		
	}

	@Override
	public List<Event> getAllEvents() {
		
		String sql="SELECT * FROM schedule;";
		List<Event> events=jdbcTemplate.query(sql, new EventRowMapper());
		return events;
	}

	@Override
	public void updateEvent(Event event, int id) {
		
		String sql="UPDATE schedule SET student_id="+
		           event.getStudent().getId()+" , exam_id="+
			       event.getExam().getId()+" , status='"+
		           event.getStatus()+"' , score="+
			       event.getScore()+" WHERE id="+id+";";
		jdbcTemplate.execute(sql);
		
	}
	private class EventRowMapper implements RowMapper<Event> {

		@Override
		public Event mapRow(ResultSet rs, int arg1) throws SQLException {
			
			Event event=new Event();
			event.setId(rs.getInt("id"));
			event.setStudent(studDao.getStudentById(rs.getInt("student_id")));
			event.setExam(examDao.getExamById(rs.getInt("exam_id")));
			event.setStatus(rs.getString("status"));
			event.setScore(rs.getInt("score"));
			return event;
		}
		
	}
	@Override
	public boolean isEventExist(Student student, Exam exam) {
		
		String sql="SELECT COUNT(score) FROM schedule WHERE student_id="+
		student.getId()+" AND exam_id='"+exam.getId()+"';";
		int result =jdbcTemplate.queryForInt(sql);
		if(result<1) return false;
		else return true;
	}

}
