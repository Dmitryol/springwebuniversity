package ru.freeman.university.models;

public class Exam {
	
	private int id;
	private String subject;
	private String date;
	
	public Exam() {
		
	}

	public int getId() {
		return id;
	}

	public String getSubject() {
		return subject;
	}

	public String getDate() {
		return date;
	}

	public void setId(int id) {
		this.id = id;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public void setDate(String date) {
		this.date = date;
	}

}
