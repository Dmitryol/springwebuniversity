package ru.freeman.university.models;

public class Event {
	
	private int id;
	private Student student;
	private Exam exam;
	private String status;
	private int score;
	
	public Event() {
		
	}

	public int getId() {
		return id;
	}

	public Student getStudent() {
		return student;
	}

	public Exam getExam() {
		return exam;
	}

	public String getStatus() {
		return status;
	}

	public int getScore() {
		return score;
	}

	public void setId(int id) {
		this.id = id;
	}

	public void setStudent(Student student) {
		this.student = student;
	}

	public void setExam(Exam exam) {
		this.exam = exam;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public void setScore(int score) {
		this.score = score;
	}

}
