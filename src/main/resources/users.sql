CREATE TABLE users(
login VARCHAR(25) PRIMARY KEY,
password VARCHAR(25) NOT NULL,
status VARCHAR(10)NOT NULL);

INSERT INTO users VALUES 
('admin','qwest123','admin'),
('dmitry','1234','examiner'),
('stud','1111','student');