<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ page session="false" %>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>students</title>
</head>
<body>
  <h3>List of students</h3>
    <c:forEach items="${students}" var="student">
            <a href='${student.id}'>
            ${student.surname} ${student.name}  
            </a>
           <br>
        </c:forEach>
   <br/>
   <a href="${pageContext.request.contextPath}/student/create">create new student</a>
   <br/><br/>  
   <a href="${pageContext.request.contextPath}/">main page</a>  
  
</body>
</html>