<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ page session="false" %>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>student</title>
<script type="text/javascript">
function setName(){
	var button=document.getElementById("name_button");
	var form=document.getElementById("name_form");
	var span=document.getElementById("name_span");
	button.hidden="true";
	span.hidden="true";
	f(form);
}
function setSurname(){
	var button=document.getElementById("surname_button");
	var form=document.getElementById("surname_form");
	var span=document.getElementById("surname_span")
	button.hidden="true";
	span.hidden="true";
	f(form);
}
function setGroup(){
	var button=document.getElementById("group_button");
	var form=document.getElementById("group_form");
	var span=document.getElementById("group_span");
	button.hidden="true";
	span.hidden="true";
	f(form);
}
function f(arg){
	var text=document.createElement("input");
	text.type="text";
	text.name="title"
	var submit=document.createElement("input");
	submit.type="submit";
	arg.appendChild(text);
	arg.appendChild(submit);
}

</script>
<style type="text/css">
.error{
	 font-size: 10px;
	 color: red;
	}
</style>
</head>
<body>
<h3>Student</h3>
<div>
<span class="error">${name_error}</span>
<br/>
<span>Name: </span>
<span id="name_span">${student.name}</span>
   <button id="name_button" onclick="setName()">edit</button>
   <form:form  id="name_form" action="setName" commandName="student" method="post" >
      <form:input path="id" hidden="true"/>
      <form:input path="name" hidden="true"/>
   </form:form>
</div>
<div>
<span class="error">${surname_error}</span>
<br/>
<span>Surname: </span>
<span id="surname_span">${student.surname}</span>
   <button id="surname_button" onclick="setSurname()">edit</button>
   <form:form  id="surname_form" action="setSurname" commandName="student" method="post" >
      <form:input path="id" hidden="true"/>
      <form:input path="surname" hidden="true"/>
   </form:form>
</div>
<div>
<span class="error">${group_error}</span>
<br/>
<span>Group: </span>
<span id="group_span">${student.group}</span>
   <button id="group_button" onclick="setGroup()">edit</button>
   <form:form  id="group_form" action="setGroup" commandName="student" method="post" >
      <form:input path="id" hidden="true"/>
      <form:input path="group" hidden="true"/>
   </form:form> 
</div>
<form:form action="delete" commandName="student" method="post">
<form:input path="id" hidden="true"/>
<input type="submit" value="delete"/>
</form:form>
<br/>
<a href="${pageContext.request.contextPath}/student/all">show all student</a>
<br/>
<a href="${pageContext.request.contextPath}/student/create">create new student</a>
<br/><br/>   
<a href="${pageContext.request.contextPath}/">main</a>
</body>
</html>