<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ page session="true" %>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
</head>
<body>
   <h1>welcome ${status}</h1>
   
   <c:if test="${status eq 'admin'}">
      <a href="${pageContext.request.contextPath}/user/create">create new user</a>
      <br/>
      <a href="${pageContext.request.contextPath}/user/all">show all users</a>
      <br/>
   </c:if>
   
   <c:if test="${status ne 'student'}">
      <a href="${pageContext.request.contextPath}/student/create">create new student</a>
      <br/>
      <a href="${pageContext.request.contextPath}/student/all">show all students</a>
      <br/>
      <a href="${pageContext.request.contextPath}/exam/create">create new exam</a>
      <br/>
      <a href="${pageContext.request.contextPath}/exam/all">show all exams</a>
      <br/>
   </c:if>
   
   <a href="${pageContext.request.contextPath}/schedule/show">schedule exams</a>
   <br/>
   <br/>
   <a href="${pageContext.request.contextPath}/logout">log out</a>
</body>
</html>