<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ page session="false" %>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<style type="text/css">
	.error{
	     font-size: 10px;
	     color: red;
	}
</style>
</head>
<body>
<h3>list of users</h3>
       <table border="1">
       <thead>
          <tr>
            <th>Name</th>
            <th>Surname</th>
            <th>Group</th>
            <th>Subject</th>
            <th>Date</th>
            <th>Status</th>
            <th>Score</th>
          </tr>          
       </thead>
       <tbody>
          <c:forEach items="${events}" var="event">
            <tr>
               <td>${event.student.name}</td>
               <td>${event.student.surname}</td>
               <td>${event.student.group}</td>
               <td>${event.exam.subject}</td>
               <td>${event.exam.subject}</td>
               <td>${event.status}</td>
               <td>${event.score}</td>
            </tr>
        </c:forEach>
      </tbody>
      </table>
      <br/>
    
      <a href="${pageContext.request.contextPath}/">main page</a>   
 
</body>
</html>