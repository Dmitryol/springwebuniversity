<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ page session="false" %>
<html>
<head>
	<title>Home</title>
	<style type="text/css">
	.error{
	    font-size: 10px;
	    color: red;
	}
	</style>
</head>
<body>
<h1>
  Log in
</h1>
<table>
<form:form action="login" method="post" commandName="user">
   <tr>  
       <td>login</td>
       <td><form:input path="login"/></td>
       <td class="error">${login_error}</td>
   </tr>
  <tr>
       <td>password</td>
       <td><form:input path="password" type="password"/></td>
       <td class="error">${password_error}</td>
  </tr>
  <tr>
       <td></td>
       <td><input type="submit" value="login"></td>
       <td></td>
</form:form>

</table>
</body>
</html>
