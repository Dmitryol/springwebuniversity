<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ page session="false" %>
<html>
<head>
	<title>Home</title>
	<style type="text/css">
	.error{
	     font-size: 10px;
	     color: red;
	}
	</style>
</head>
<body>
  <h1>
     Create user
  </h1>
  <table>
    <form:form commandName="user" method="post">
    <tr>
       <td>login</td>
       <td><form:input path="login"/></td>
       <td class="error">${login_error}</td> 
    </tr>
    <tr>
       <td>password</td>
       <td><form:input path="password"/></td>
       <td class="error">${password_error}</td> 
    </tr>
    <tr>
       <td>status</td>
       <td>
           <form:select path="status">
               <form:option value="admin">admin</form:option>
               <form:option value="examiner">examiner</form:option>
               <form:option value="student">student</form:option>
           </form:select>
       </td>
       <td class="error">${status_error}</td>
    </tr>
    <tr>
       <td></td>
       <td><input type="submit" value="create"></td>
       <td></td>
    </tr>
    </form:form>
  </table>
    <a href="${pageContext.request.contextPath}/user/all">show all users</a>
    <br/><br/>
    <a href="${pageContext.request.contextPath}/">main page</a> 
</body>
</html>
