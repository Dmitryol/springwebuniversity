<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ page session="false" %>
<html>
<head>
	<title>Home</title>
	<style type="text/css">
	.error{
	     font-size: 10px;
	     color: red;
	}
	</style>
</head>
<body>
  <h1>
     Create student
  </h1>
  <table>
    <form:form commandName="student" method="post">
    <tr>
       <td>name</td>
       <td><form:input path="name"/></td>
       <td class="error">${name_error}</td> 
    </tr>
    <tr>
       <td>surname</td>
       <td><form:input path="surname"/></td>
       <td class="error">${surname_error}</td> 
    </tr>
    <tr>
       <td>group</td>
       <td><form:input path="group"/></td>
       <td class="error">${group_error}</td>
    </tr>
    <tr>
       <td></td>
       <td><input type="submit" value="create"></td>
       <td></td>
    </tr>
    </form:form>
  </table>
  <a href="${pageContext.request.contextPath}/student/all">show all students</a>
  <br/><br/>
  <a href="${pageContext.request.contextPath}/">main page</a> 
</body>
</html>