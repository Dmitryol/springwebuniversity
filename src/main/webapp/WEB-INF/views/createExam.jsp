<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ page session="false" %>
<!DOCTYPE html>
<html>
<head>
	<title>create exam</title>
<style type="text/css">
.error{
	font-size: 10px;
	color: red;
}
</style>
</head>
<body>
   <h1>
      Create exam
   </h1>
   <table>
      <form:form commandName="exam" method="post">
      <tr>
         <td>subject</td>
         <td><form:input path="subject"/></td>
         <td class="error">${subject_error}</td> 
      </tr>
      <tr>
         <td>surname</td>
         <td><form:input type="date" value="2018-01-01" path="date"/></td>
         <td class="error"></td> 
      </tr>
      <tr>
         <td></td>
         <td><input type="submit" value="create"></td>
         <td></td>
      </tr>
      </form:form>
   </table>
   <a href="${pageContext.request.contextPath}/exam/all">show all exam</a>
   <br/><br/>
   <a href="${pageContext.request.contextPath}/">main page</a> 
</body>
</html>