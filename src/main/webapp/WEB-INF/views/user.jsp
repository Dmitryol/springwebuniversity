<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ page session="false" %>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<script type="text/javascript">
function setLogin(){
	var form=document.getElementById("login_form");
	var button=document.getElementById("login_button");
	var span=document.getElementById("login_span");
	button.hidden="true";
	span.hidden="true";
	f(form);
}
function setPassword(){
	var form=document.getElementById("password_form");
	var button=document.getElementById("password_button");
	var span=document.getElementById("password_span");
	button.hidden="true";
	span.hidden="true";
	f(form);
}
function setStatus(){
	var button=document.getElementById("status_button");
	var form=document.getElementById("status_form");
	var span=document.getElementById("status_span");
	
	button.hidden="true";
	
 	var select=document.createElement("select");
 	select.name="status"
	
 	var admin=document.createElement("option");
 	var examinator=document.createElement("option");
 	var student=document.createElement("option");
 	
 	admin.value="admin";
 	examinator.value= "examiner";
 	student.value= "student";
 	
 	admin.innerHTML = "admin";
 	examinator.innerHTML = "examiner";
 	student.innerHTML = "student";
	
 	select.appendChild(admin);
 	select.appendChild(examinator);
 	select.appendChild(student);
	
	var submit=document.createElement("input");
	submit.type="submit";
	form.appendChild(select);
	form.appendChild(submit);
}

function f(arg){
	var text=document.createElement("input");
	text.type="text";
	text.name="title"
	var submit=document.createElement("input");
	submit.type="submit";
	arg.appendChild(text);
	arg.appendChild(submit);
}

</script>
	<style type="text/css">
	.error{
	     font-size: 10px;
	     color: red;
	}
	</style>
</head>
<body>
<h3>User</h3>
<div>
<span class="error">${login_error }</span> 
<br/> 
<span>login: </span>
<span id="login_span">${user.login}</span>
   <button id="login_button" onclick="setLogin()">edit</button>
   <form:form  id="login_form" action="setLogin" commandName="user" method="post" >
      <form:input path="login" hidden="true"/>
   </form:form>
</div> 
<div>
<span class="error">${password_error }</span> 
<br/>
<span>password: </span>
<span id="password_span">${user.password}</span>
   <button id="password_button" onclick="setPassword()">edit</button>
   <form:form  id="password_form" action="setPassword" commandName="user" method="post" >
      <form:input path="login" hidden="true"/> 
      <form:input path="password" hidden="true"/> 
   </form:form>
</div> 

<span class="error">${status_error}</span>
<br/>
<span>status: </span>
<span id="status_span">${user.status}</span>
   <button id="status_button" onclick="setStatus()">edit</button>
   <form:form  id="status_form" action="setStatus" commandName="user" method="post" >
      <form:input path="login" hidden="true"/> 
   </form:form> 
<br/>
<span class="error">${delete_error }</span>
<form:form action="delete" commandName="user" method="post">
<form:input path="login" hidden="true"/>
<form:input path="password" hidden="true"/>
<form:input path="status" hidden="true"/>
<input type="submit" value="delete"/>
</form:form>
<br/>
<a href="${pageContext.request.contextPath}/user/all">show all users</a>
<br/>
<a href="${pageContext.request.contextPath}/user/create">create new user</a>
<br/><br/>
<a href="${pageContext.request.contextPath}/">main page</a>
   
</body>
</html>