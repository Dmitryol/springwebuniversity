<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ page session="false" %>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<style type="text/css">
	.error{
	     font-size: 10px;
	     color: red;
	}
</style>
</head>
<body>
<h3>list of users</h3>
       <table border="1">
       <thead>
          <tr>
            <th>Name</th>
            <th>Surname</th>
            <th>Group</th>
            <th>Subject</th>
            <th>Date</th>
            <th>Status</th>
            <th>Score</th>
          </tr>          
       </thead>
       <tbody>
          <c:forEach items="${events}" var="event">
            <tr>
               <td>${event.student.name}</td>
               <td>${event.student.surname}</td>
               <td>${event.student.group}</td>
               <td>${event.exam.subject}</td>
               <td>${event.exam.subject}</td>
               <td>${event.status}</td>
               <td>${event.score}</td>
               <td>
                   <form action="delete" method="post">
                      <input type="number"  value="${event.id }" name="id" hidden="true"/>
                      <input type="submit" value="X"/>
                   </form>
               </td>
            </tr>
        </c:forEach>
      </tbody>
      </table>
      <br/>
      <span class="error">${error}</span>
      <form:form commandName="event" method="post">
        <span>student</span>
        <form:select path="student.id">
           <c:forEach items="${students}" var="student">
               <form:option value="${student.id}">${student.surname} ${student.name }</form:option>
           </c:forEach>
        </form:select>
        <span>exam</span>
        <form:select path="exam.id">
           <c:forEach items="${exams}" var="exam">
               <form:option value="${exam.id}">${exam.subject}</form:option>
           </c:forEach>
        </form:select>
        <span>status</span>
        <form:select path="status">
           <form:option value="passed">passed</form:option>
           <form:option value="not passed">not passed</form:option>
           <form:option value="admitted">admitted</form:option>
           <form:option value="not admitted">not admitted</form:option>       
        </form:select>
        <span>score</span>
         <form:select path="score">
           <form:option value="5">5</form:option>
           <form:option value="4">4</form:option>
           <form:option value="3">3</form:option>
           <form:option value="2">2</form:option>  
           <form:option value="1">1</form:option>     
           <form:option value="0">0</form:option> 
        </form:select>   
        <input type="submit">
      </form:form>
      <a href="${pageContext.request.contextPath}/">main page</a>   
 
</body>
</html>