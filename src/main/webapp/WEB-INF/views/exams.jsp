<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ page session="false" %>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
</head>
<body>
   <h3>list of exams</h3>
        <c:forEach items="${exams}" var="exam">
            <a href='${exam.id}'>
            ${exam.subject}  
            </a>
           <br>
        </c:forEach>
   <br/>
   <a href="${pageContext.request.contextPath}/exam/create">create new exam</a>
   <br/><br/>  
   <a href="${pageContext.request.contextPath}/">main</a>   
</body>
</html>