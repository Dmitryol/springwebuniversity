<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ page session="false" %>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>exam</title>
<script type="text/javascript">

     function setSubject(){
    	 var span=document.getElementById("subject_span")
    	 var form=document.getElementById("subject_form")
    	 var button=document.getElementById("subject_button");
    	 span.hidden="true";
    	 button.hidden="true";    	 
    	 f(form, "text");
     }
     function setDate(){
    	 var span=document.getElementById("date_span");
    	 var form=document.getElementById("date_form");
    	 var button=document.getElementById("date_button")
    	 span.hidden="true";
    	 button.hidden="true";
    	 f(form, "date");
     }
     function f(arg, type){
    		var text=document.createElement("input");
    		text.type=type;
    		text.name="title"
    		var submit=document.createElement("input");
    		submit.type="submit";
    		arg.appendChild(text);
    		arg.appendChild(submit);
    	}
</script>
<style type="text/css">
.error{
	 font-size: 10px;
	 color: red;
	}
</style>
</head>
<body>
<h3>Exam</h3>
<div>
<span class="error">${subject_error}</span>
<br/>
<span>Subject: </span>
<span id="subject_span">${exam.subject}</span>
   <button id="subject_button" onclick="setSubject()">edit</button>
   <form:form  id="subject_form" action="setSubject" commandName="exam" method="post" >
      <form:input path="id" hidden="true"/> 
      <form:input path="subject" hidden="true"/> 
   </form:form>
</div> 
<div>
<span class="error">${date_error}</span>
<br/>
<span>date: </span>
<span id="date_span">${exam.date}</span>
   <button id="date_button" onclick="setDate()">edit</button>
   <form:form  id="date_form" action="setDate" commandName="exam" method="post" >
      <form:input path="id" hidden="true"/> 
      <form:input path="date" hidden="true"/> 
   </form:form>
</div> 
<form:form action="delete" commandName="exam" method="post">
<form:input path="id" hidden="true"/>
<input type="submit" value="delete"/>
</form:form>
<br/>
<a href="${pageContext.request.contextPath}/exam/all">show all exams</a>
<br/>
<a href="${pageContext.request.contextPath}/exam/create">create new exam</a>
<br/><br/>   
<a href="${pageContext.request.contextPath}/">main page</a>
   
</body>
</html>